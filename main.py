import sys
import argparse
from kafka_client import KafkaClient


def createParser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')

    produce_parser = subparsers.add_parser('produce')
    produce_parser.add_argument('-m', '--message', required=True)
    produce_parser.add_argument('-t', '--topic', required=True)
    produce_parser.add_argument('-k', '--kafka', required=True)

    consume_parser = subparsers.add_parser('consume')
    consume_parser.add_argument('-t', '--topic', required=True)
    consume_parser.add_argument('-k', '--kafka', required=True)

    return parser


def produce(namespace):
    k = KafkaClient(namespace.kafka)
    k.send_message(namespace.topic, namespace.message)
    print(f'Message "{namespace.message}" produced into "{namespace.topic}". Kafka host: {namespace.kafka}')


def consume(namespace):
    k = KafkaClient(namespace.kafka)
    try:
        while True:
            msg = k.get_message(namespace.topic)
            if msg:
                print(msg)
    except Exception as e:
        raise e


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args()

    if namespace.command == 'produce':
        produce(namespace)
    elif namespace.command == 'consume':
        consume(namespace)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
