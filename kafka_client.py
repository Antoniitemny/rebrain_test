import sys
from typing import Optional

from confluent_kafka import (Producer, Consumer, KafkaError, KafkaException,
                             Message)

class KafkaClient(object):
    def __init__(self, broker_servers, group_id='listener-1'):
        self.broker_servers = broker_servers
        self.group_id = group_id
        self.__producer = None
        self.__consumer = None

    @property
    def producer(self) -> Producer:
        try:
            if self.__producer:
                return self.__producer
            self.__producer = Producer(
                {'bootstrap.servers': self.broker_servers})
            return self.__producer
        except Exception as e:
            print('kafka_alarm in __create_producer')
            raise e

    @property
    def consumer(self) -> Consumer:
        try:
            if self.__consumer:
                return self.__consumer
            self.__consumer = Consumer(
                {'bootstrap.servers': self.broker_servers,
                 'group.id': self.group_id, 'auto.offset.reset': 'earliest'})
            return self.__consumer
        except Exception as e:
            print('kafka_alarm in __create_consumer')
            raise e

    def __get_message(self) -> Optional[Message]:
        tries = 0
        while tries < 3:
            message = self.consumer.poll(timeout=1.0)
            if message is None:
                tries += 1
                continue
            else:
                return message
        return None

    def get_message(self, topic: str) -> Optional[str]:
        try:
            self.consumer.subscribe([topic])
            message: Message = self.__get_message()
            if message is None:
                return None
            elif message.error():
                self.__get_message_error_handler(message)
            else:
                value = message.value().decode('utf-8')
                return value
        finally:
            # Close down consumer to commit final offsets.
            self.consumer.close()
            self.__consumer = None

    @staticmethod
    def __get_message_error_handler(message: Message) -> None:
        if message.error().code() == KafkaError._PARTITION_EOF:
            sys.stderr.write('%% %s [%d] reached end at offset %d\n' % (
                message.topic(), message.partition(), message.offset()))
        elif message.error():
            raise KafkaException(message.error())

    def send_message(self, topic: str, message: str) -> None:
        try:
            print(type(message))
            self.producer.produce(topic, message)
            self.producer.flush()
        except Exception as e:
            print('kafka_alarm in kafka_send_data')
            raise e


if __name__ == '__main__':
    broker_servers = 'localhost:9092'
    client = KafkaClient(broker_servers)
    msg = client.get_message('test_topic')
    print(msg)